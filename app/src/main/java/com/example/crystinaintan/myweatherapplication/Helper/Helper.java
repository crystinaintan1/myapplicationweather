package com.example.crystinaintan.myweatherapplication.Helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Helper {
    static String stream = null;

    public Helper(){

    }

    public String getHTTPData(String urlLink)
    {
        try {
            URL url = new URL(urlLink);
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
            if(httpURLConnection.getResponseCode() == 200)
            {
                InputStreamReader i = new InputStreamReader(httpURLConnection.getInputStream());
                BufferedReader r = new BufferedReader(i);
                StringBuilder sb = new StringBuilder();
                String line;
                while((line = r.readLine())!= null)
                {
                    sb.append(line);
                    stream = sb.toString();
                    httpURLConnection.disconnect();
                }
            }
            else if(httpURLConnection.getResponseCode() == 404)
            {
                stream = "Error Not found city";
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream;
    }

}
