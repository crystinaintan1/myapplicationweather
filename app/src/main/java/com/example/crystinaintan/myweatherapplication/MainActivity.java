package com.example.crystinaintan.myweatherapplication;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.crystinaintan.myweatherapplication.Common.cuaca;
import com.example.crystinaintan.myweatherapplication.Helper.Helper;
import com.example.crystinaintan.myweatherapplication.Model.OpenWeather;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoProvider;

import java.lang.reflect.Type;

import static android.webkit.ConsoleMessage.MessageLevel.LOG;

public class MainActivity extends AppCompatActivity implements LocationListener {

    TextView txtcity, txtLastUpdate, txtDescription, txtHumadity, txtTime, txtinfo, txtCelcius;
    EditText nameCityInput;
    ImageView imgView;
    Location location;

    LocationManager locationManager;
    String provider;
    static double lat, lng;
    OpenWeather openWeatherMap = new OpenWeather();

    int MY_PERMISSION = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        txtcity = this.findViewById(R.id.txtcity);
        txtCelcius = this.findViewById(R.id.txtCelcius);
        txtDescription = this.findViewById(R.id.txtDescription);
        txtHumadity = this.findViewById(R.id.txtHumadity);
        txtLastUpdate = this.findViewById(R.id.txtLastUpdate);
        txtTime = this.findViewById(R.id.txtTime);
        txtinfo = this.findViewById(R.id.txtStatus);
        nameCityInput = (EditText)findViewById(R.id.inputCity);
        imgView = this.findViewById(R.id.imageView);



        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        provider = locationManager.getBestProvider(new Criteria(), false);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                Manifest.permission.INTERNET,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_NETWORK_STATE,
                    Manifest.permission.SYSTEM_ALERT_WINDOW,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            },MY_PERMISSION);
        }
         location = locationManager.getLastKnownLocation(provider);
        if(location == null)
        {
            Log.e("TAG","No Location");
        }

        nameCityInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                onLocationChanged(location);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                    Manifest.permission.INTERNET,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_NETWORK_STATE,
                    Manifest.permission.SYSTEM_ALERT_WINDOW,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            },MY_PERMISSION);
        }
        locationManager.removeUpdates(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                    Manifest.permission.INTERNET,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_NETWORK_STATE,
                    Manifest.permission.SYSTEM_ALERT_WINDOW,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            },MY_PERMISSION);
        }
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }

    @Override
    public void onLocationChanged(Location location) {

//        lat = location.getLatitude();
//        lng = location.getLongitude();
       String lat = nameCityInput.getText().toString();
        lng = 139;

        new GetWeather().execute(cuaca.apiRequest(String.valueOf(lat),String.valueOf(lng)));
        Log.e("TAG",lat+" =>> Ini adalah Lat");
        Log.e("TAG",lng+" =>> Ini adalah lng");

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private class GetWeather extends AsyncTask<String, Void, String>{

        ProgressDialog pd = new ProgressDialog(MainActivity.this);
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setTitle("Please Wait...");
            pd.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s.contains("Error Not found city"))
            {
                pd.dismiss();
                txtinfo.setText(String.format("%s", "Wrong City Name! please complete the city name.. "));
                return;
            }
            Gson gson = new Gson();
            Type mType = new TypeToken<OpenWeather>(){}.getType();
            openWeatherMap = gson.fromJson(s,mType);
            pd.dismiss();

            txtcity.setText(String.format("%s,%s", openWeatherMap.getName(), openWeatherMap.getSys().getCountry()));
            txtLastUpdate.setText(String.format("Last Updated: %s", cuaca.getDateNow()));
            txtDescription.setText(String.format("%s",openWeatherMap.getWeather().get(0).getDescription()));
            txtHumadity.setText(String.format("%d%%",openWeatherMap.getMain().getHumaditiy()));
            txtTime.setText(String.format("%s/%s", cuaca.unixTimeStampToDateTime(openWeatherMap.getSys().getSunrise()),cuaca.unixTimeStampToDateTime(openWeatherMap.getSys().getSunset())));
            //double tempCel = (openWeatherMap.getMain().getTemp()-32.0)*0.55;
            txtCelcius.setText(String.format("%.2f °C",openWeatherMap.getMain().getTemp()));
            txtinfo.setText(String.format("%s","Have a Nice Day!!"));

            Picasso.get().load(cuaca.getImage(openWeatherMap.getWeather().get(0).getIcon()))
                    .into(imgView);
        }

        @Override
        protected String doInBackground(String... params) {
           String stream = null;
           String urlString = params[0];
           Helper help = new Helper();
           stream = help.getHTTPData(urlString);
           return stream;

        }
    }
}
